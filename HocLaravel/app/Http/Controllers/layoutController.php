<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\product;
class layoutController extends BaseController
{
	function __construct()
	{
		$products=product::all();
		view()->share('products',$products);
	}
   function Showsanpham(){
   	return view('layout.sanpham');
   }
   function chitietsanpham($id){
   	 $chitiet=product::where('id',$id)->get();
   	  view()->share('chitiet',$chitiet);
   	 return view('layout.chitietsanpham');
   }
}