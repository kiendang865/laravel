<!DOCTYPE html>
<html>
<head>
	<title>Giao Diện</title>
	<link rel="stylesheet" type="text/css" href="/HocLaravel/public/css/style.css">
</head>
<body>

	<div style="border: 1px solid black;" class="wrapper">
		@include ('layout.header')
		@include('layout.menu')
		@include('layout.detail')
	</div>
</body>
</html>