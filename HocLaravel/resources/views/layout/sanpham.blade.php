<!DOCTYPE html>
<html>
<head>
	<title>Giao Diện</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

	<div style="border: 1px solid black;" class="wrapper">
		@include ('layout.header')
		@include('layout.menu')
		@include('layout.content')
	</div>
</body>
</html>