<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});
// Route::get('/sanpham','sanpham@Showsanpham');
// Route::get('/chitiet','chitiet_Controller@Showchitiet');
Route::get('sanpham','layoutController@Showsanpham');
Route::get('chitietsanpham/{id}','layoutController@chitietsanpham');